const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');
//Init App
const app = express();

// Mongodb Connection

mongoose.connect('mongodb://localhost/mycustomers');
let db = mongoose.connection;

// Check Connection
db.once('open',function(){
	console.log('Connected to Mongodb');
})

// Db Errors
db.on('error',function(err){
console.log(err);
});


// Public folder path

app.use(express.static(path.join(__dirname,'public')));

//Load View Engine


app.set('views',path.join(__dirname,'views'));

app.set('view engine','pug');

app.use(bodyParser.urlencoded({extended:false}));

app.use(bodyParser.json());


//Models
let articles = require('./models/articles');

// Routes --START
app.get('/',function(req, res){
articles.find({},function(err, articles){
	if(err){
		console.log(err);
	}
	else{
		res.render('index',{
		articles:articles
		});		
	}
});

});

app.get('/articles/add',function(req,res){

	res.render('add-articles',{
		title:"Add Articles"
	});
});


app.post('/articles/add',function(req,res){
	let article = new Article();
	article.title = req.body.title;
	article.author = req.body.author;
	article.body = req.body.body;


});

// Routes --END

// start server

app.listen('3000',function(){
	console.log('Server started on port 3000');
});

